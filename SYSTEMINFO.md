# Cisco ACS

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Access Control System
Product Page: https://www.cisco.com/c/en/us/support/security/secure-access-control-system/series.html

## Introduction
We classify Cisco ACS into the Security domain as Cisco ACS is a centralized authentication, authorization, and accounting solution for managing network access and security policies.

## Why Integrate
The Cisco ACS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco ACS.With this adapter you have the ability to perform operations with Cisco ACS on items such as:

- Query
- Identity
- Network Devicesz

## Additional Product Documentation
The [API documents for Cisco ACS](https://www.cisco.com/c/en/us/td/docs/net_mgmt/cisco_secure_access_control_system/5-8/sdk/acs_sdk/rest.html)
