
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:58PM

See merge request itentialopensource/adapters/adapter-cisco_acs!15

---

## 0.4.4 [08-25-2024]

* fix vulnerability

See merge request itentialopensource/adapters/adapter-cisco_acs!13

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_acs!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:18PM

See merge request itentialopensource/adapters/adapter-cisco_acs!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:52PM

See merge request itentialopensource/adapters/adapter-cisco_acs!10

---

## 0.4.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-cisco_acs!9

---

## 0.3.5 [03-27-2024]

* Changes made at 2024.03.27_13:50PM

See merge request itentialopensource/adapters/security/adapter-cisco_acs!8

---

## 0.3.4 [03-13-2024]

* update vendor

See merge request itentialopensource/adapters/security/adapter-cisco_acs!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_10:56AM

See merge request itentialopensource/adapters/security/adapter-cisco_acs!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:19PM

See merge request itentialopensource/adapters/security/adapter-cisco_acs!5

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:55AM

See merge request itentialopensource/adapters/security/adapter-cisco_acs!4

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_acs!3

---
